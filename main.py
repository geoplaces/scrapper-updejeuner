import logging.config
from os import getenv as ge

import click
import requests
import sentry_sdk
import yaml
from dotenv import load_dotenv

from app.ScrapperDejupApi import get_scrapper as get_dejup_scrapper
from app.ScrapperEdenredApi import get_scrapper as get_edenred_scrapper

load_dotenv()


@click.command()
@click.option('-u', '--update', type=int, default=0)
@click.option('--source', '-s', type=str, default="dejup")
def main(update, source):
    dsn = ge('SENTRY_DSN')
    if dsn is not None and dsn != '':
        sentry_sdk.init(
            dsn=dsn,
            # Set traces_sample_rate to 1.0 to capture 100%
            # of transactions for performance monitoring.
            traces_sample_rate=1.0,
            # Set profiles_sample_rate to 1.0 to profile 100%
            # of sampled transactions.
            # We recommend adjusting this value in production.
            profiles_sample_rate=1.0,
        )
    with open("logging.yml", "r") as f:
        yaml_config = yaml.safe_load(f.read())
        logging.config.dictConfig(yaml_config)

    logger = logging.getLogger('app.main')
    try:
        if source == "dejup":
            scrapper = get_dejup_scrapper(bool(update))
        elif source == "edenred":
            scrapper = get_edenred_scrapper(bool(update))
        else:
            raise NameError(f"Unknown source: {source}")
        scrapper.update_locations(update)
        scrapper.scrap()

        heart_beat_url = ge('HEART_BEAT_URL')
        if len(scrapper.errors) != 0:
            logger.warning(f'Scrap errors: {scrapper.errors}')
        if len(scrapper.errors) == 0 and heart_beat_url is not None and heart_beat_url != '':
            resp = requests.get(heart_beat_url)
            if resp.status_code != 200:
                logger.warning(f'Heart beat failed with {resp.status_code}')
    except:
        logger.exception("Error during scrapping")


if __name__ == "__main__":
    main()
