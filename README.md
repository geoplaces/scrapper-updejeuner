# Scrapper

Scraps upDejeauner/Edenred website for places and store results with
the API call.

## Usage

Requirements install

```bash
python3 -m venv vdb
. vdb/bin/activate
pip install -r requirements.txt
```

Scrapper call

```bash
python main.py
```

If you want to search indecies that were already searched but say 6 days ago use

```bash
python main.py --update=6
```

The source of addresses for scrap can be specified with `--source`

```bash
python main.py --source=edenred
python main.py --source=dejup # default
```