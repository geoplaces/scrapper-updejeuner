import logging
from datetime import datetime
from os import getenv as ge
from time import sleep
from typing import Dict, Iterable, List

from dotenv import load_dotenv
from selenium.common import exceptions as ex
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
from seleniumwire import webdriver

from app.ApiClient import ApiClient
from app.ScrapperBase import ScrapperBase


def wait_load(driver, by_parameter, name):
    """Helper function to wait for element load."""
    wait = WebDriverWait(driver, 10)
    return wait.until(ec.presence_of_element_located((by_parameter, name)))


def get_scrapper(update: bool = False) -> ScrapperBase:
    """Build a scrapper."""
    load_dotenv()
    scrap_url = ge('DEJUP_SCRAP_URL')
    reg_sleep = ge('REGULAR_SLEEP')
    page_sleep = ge('PAGINATION_SLEEP')
    driver_path = ge('DRIVER_PATH')

    scrapper = ScrapperDejup(scrap_url, int(reg_sleep), int(page_sleep), driver_path, update)

    return scrapper


class ScrapperDejup(ScrapperBase):
    def __init__(self, href: str, reg_sleep: int, page_sleep: int, driver_path: str, update: bool):
        super().__init__(href, reg_sleep, page_sleep)
        self.data: List = []
        self.bunch_size = 20
        self.update: bool = update

        self.driver_path: str = driver_path
        self.driver: webdriver = None

        self.stored = 0

        self.logger = logging.getLogger('app.ScrapperDejup')
        self.logger.info('ScrapperDejup init ok')

        self.storage = ApiClient(ge('URL'), "/dejup", ge('API_CLIENT'), ge('API_PASSW'))
        if not self.storage.initialise():
            self.logger.error('Failed to init API client.')
            raise RuntimeError('Failed to initialize API client')

    def prepare(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument("--disable-dev-shm-usage")
        service = Service(executable_path=self.driver_path)
        self.driver = webdriver.Chrome(service=service, options=options)
        self.logger.debug('Prepared')

    def starting_point(self):
        self.driver.get(self.start_href)
        wait_load(self.driver, By.CLASS_NAME, 'carte4C2')
        # Accepting cookies
        # agree_button = wait_load(self.driver, By.ID, 'didomi-notice-agree-button')
        # self.driver.execute_script("arguments[0].scrollIntoView();", agree_button)
        # agree_button.click()
        # self.logger.debug('Agreed with cookies')
        self.logger.debug('Passed starting point')


    def next_search(self, location):
        self.logger.info(f'Make a new search of {location}')
        self.driver.get(self.start_href)
        card = wait_load(self.driver, By.CLASS_NAME, 'carte4C2')

        try:
            card.find_element(By.TAG_NAME, 'label').click()
        except ex.ElementNotInteractableException:
            pass

        wait_load(self.driver, By.CSS_SELECTOR, '.bloc.bloc-2.search')

        self.logger.debug('Prepared for search')
        # show search panel
        try:
            search_button_block = wait_load(self.driver, By.CSS_SELECTOR, '.bloc.bloc-2.search')
            search_button = search_button_block.find_elements(By.CSS_SELECTOR, '.btn.submit')

            field = search_button_block.find_element(By.ID, 'searchtext')
            field.clear()
            field.send_keys(str(location))
            search_button[1].click()
        except (ex.ElementClickInterceptedException, ex.NoSuchElementException,
                ex.TimeoutException, ex.ElementNotInteractableException):
            sleep(self.regular_sleep)
            search_button_block = wait_load(self.driver, By.CSS_SELECTOR, '.bloc.bloc-2.search')
            search_button = search_button_block.find_elements(By.CSS_SELECTOR, '.btn.submit')

            field = search_button_block.find_element(By.ID, 'searchtext')
            field.clear()
            field.send_keys(str(location))
            search_button[1].click()

        # search results wait conditions
        wait_load(self.driver, By.CLASS_NAME, 'em-page-nav__nav-links')

        self.logger.info(f'Search with location {location} ok')

    def get_main_container(self) -> Iterable:
        sleep(self.pagination_sleep)
        cont = wait_load(self.driver, By.CLASS_NAME, 'agencies')
        return cont

    def get_elements(self, container: WebElement) -> Dict:
        for element in container.find_elements(By.TAG_NAME, "li"):
            yield element

    def store_element(self, element: WebElement):
        self.logger.debug('Store element')
        try:
            address = element.find_element(By.CLASS_NAME, 'agencyaddress')
            place_id = element.get_attribute('id')
            link = element.get_attribute('rel')
            name = element.find_element(By.CLASS_NAME, 'seolink').text
        except ex.StaleElementReferenceException:
            # workaround selenium crash
            sleep(1)
            address = element.find_element(By.CLASS_NAME, 'agencyaddress')
            place_id = element.get_attribute('id')
            link = element.get_attribute('rel')
            name = element.find_element(By.CLASS_NAME, 'seolink').text

        address_text = address.text
        street = address_text.split('\n')[0]
        city = address_text.split('\n')[1]
        zipcode = city.split(' ')[0]
        city = ' '.join(city.split(' ')[1:])

        self.data.append(
                {
                    'id'        : place_id,
                    'link'      : link,
                    'name'      : name,
                    'address'   : street,
                    'zipcode'   : zipcode,
                    'city'      : city,
                    'updated_at': datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
                }
        )

        if len(self.data) >= self.bunch_size:
            self.logger.debug('Store elements with the API call')
            if self.update:
                self.storage.update_element_request(self.data)
            else:
                self.storage.store_element_request(self.data)
            self.stored += len(self.data)
            self.data = []

        self.logger.debug('Store element ok')

    def next_page(self) -> bool:
        self.logger.debug('Go to next page')
        pagination = self.driver.find_element(By.CLASS_NAME, 'em-page-nav__nav-links')
        # scroll to pagination
        self.driver.execute_script("arguments[0].scrollIntoView();", pagination)

        index = -1
        pages = pagination.find_elements(By.TAG_NAME, "a")
        for num, page in enumerate(pages):
            if page.get_attribute('class') == 'em-page-nav__current':
                index = num + 1
                break
        self.logger.debug(f'Page {pages[index - 1].text} done')
        # Reach end of the pages
        if index == len(pages):
            self.logger.info('Search over pages done')
            return False

        pages[index].click()
        wait_load(self.driver, By.CLASS_NAME, 'agencies')
        self.logger.debug('Go to next page ok')
        return True

    def finish_location(self, location) -> bool:
        self.logger.info(f'Finishing location `{location}`')

        if len(self.errors) > 0:
            self.logger.warning('Errors occurred during scrap. Do not finalize location.')
            return True

        if self.update:
            if not self.storage.update_element_request(self.data):
                return False
        else:
            if not self.storage.store_element_request(self.data):
                return False

        self.stored += len(self.data)
        self.data = []

        if not self.storage.finish_location_request(location):
            return False

        self.logger.info(f'Finishing location `{location}` ok, new elements {self.stored}')
        self.stored = 0
        return True

    def update_locations(self, update):
        self.logger.info(f'Updating location (update={update})')
        response = self.storage.update_locations_request(update)
        for item in response:
            self.search_locations.append(item['index'])

        self.logger.info(
                f'Updating locations ok, size = `{len(self.search_locations)}`')

    def finalise_search(self):
        self.driver.quit()
