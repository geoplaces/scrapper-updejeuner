import logging
from datetime import datetime
from os import getenv as ge
from time import sleep
from typing import List, Dict, Iterable

import requests
from dotenv import load_dotenv

from app.ApiClient import ApiClient
from app.ScrapperBase import ScrapperBase


def get_scrapper(update: bool = False) -> ScrapperBase:
    """Build a scrapper."""
    load_dotenv()
    scrap_url = ge('EDENRED_SCRAP_URL')
    reg_sleep = ge('REGULAR_SLEEP')
    page_sleep = ge('PAGINATION_SLEEP')

    scrapper = ScrapperEdenred(scrap_url, int(reg_sleep), int(page_sleep), update)
    return scrapper


class ScrapperEdenred(ScrapperBase):
    def __init__(self, scrap_url: str, reg_sleep: int, page_sleep: int, update: bool):
        super().__init__(scrap_url, reg_sleep, page_sleep)

        self.start_href += '/api/outlets'

        self.data: List = []
        self.bunch_size = 200
        self.update: bool = update

        self.logger = logging.getLogger('app.ScrapperEdenred')

        self.args = {}
        self.last_page = False

        self.stored = 0
        self.storage = ApiClient(ge('URL'), "/edenred", ge('API_CLIENT'), ge('API_PASSW'))
        if not self.storage.initialise():
            self.logger.error('Failed to init API client.')
            raise RuntimeError('Failed to initialize API client')

        self.logger.info('ScrapperEdenred init ok')

    def parse_item(self, place: Dict) -> Dict:
        result = {}
        try:
            result = {
                'id': place['store_ref'],
                'name': place['title'],
                'address': "",
                'zipcode': int(place['address']['zip_code']),
                'city': place['address']['city_name'],
                'lat': place['address']['latitude'],
                'long': place['address']['longitude'],
                'categoryName': "",
                'updated_at': datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
            }
        except KeyError:
            self.logger.error(f'Error parsing place: {place}')
            self.errors.append(f'Failed to process response {place}')
        if place.get('cookshops'):
            result['categoryName'] = ', '.join(place['cookshops'])
        if place['address'].get('address_line_1'):
            result['address'] = place['address'].get('address_line_1')
        return result

    def store_data(self):
        self.logger.debug('Store elements with the API call')
        if self.update:
            if not self.storage.update_element_request(self.data):
                self.errors.append(f'Failed to update elements {self.data}')
        else:
            if not self.storage.store_element_request(self.data):
                self.errors.append(f'Failed to post elements {self.data}')
        self.stored += len(self.data)
        self.data = []

    def prepare_for_search(self):
        self.args = {
            'result_level': 'full',
            'page_size': 200,
            'supported_products': 'CTR_4C',
            'page_index': 0
        }

    def next_search(self, location):
        self.args['location'] = str(location['lat']) + "," + str(location['lon'])
        self.args['radius'] = location['radius']
        self.args['page_index'] = 0

    def get_main_container(self) -> Iterable:
        self.logger.debug(f'Perform a request to {self.start_href} with args {self.args}')
        resp = requests.get(self.start_href, params=self.args)
        if resp.status_code != 200:
            self.logger.error(f'Scrap target response for: `{self.args}` with: {resp.status_code}')
            self.errors.append(f'Failed to scrap with args: {self.args}')
            self.last_page = True
            return []
        self.logger.debug(f'Update records {len(resp.json())}')

        if len(resp.json()) < self.args['page_size']:
            self.last_page = True

        return resp.json()

    def get_elements(self, container: Iterable):
        for place in container:
            yield place

    def store_element(self, element: Dict):
        self.data.append(self.parse_item(element))
        if len(self.data) >= self.bunch_size:
            self.store_data()

    def next_page(self) -> bool:
        if self.last_page:
            self.store_data()
            return False

        sleep(self.pagination_sleep)
        self.args['page_index'] += 1
        return True

    def update_locations(self, update):
        self.logger.info(f'Updating locations (update={update})')
        response = self.storage.update_locations_request(update)
        for item in response:
            self.search_locations.append(item)

        self.logger.info(
            f'Updating locations ok, size = `{len(self.search_locations)}`')

    def finish_location(self, location) -> bool:
        self.logger.info(f'Finishing location `{location}`')

        if len(self.errors) > 0:
            self.logger.warning('Errors occurred during scrap. Do not finalize location.')
            return True

        if self.update:
            if not self.storage.update_element_request(self.data):
                return False
        else:
            if not self.storage.store_element_request(self.data):
                return False
        self.stored += len(self.data)
        self.data = []

        if not self.storage.finish_location_request(location["index"]):
            return False
        self.logger.info(f'Finishing location `{location["index"]}` ok, new elements {self.stored}')
        self.stored = 0
        return True
