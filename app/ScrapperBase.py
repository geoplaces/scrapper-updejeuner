import logging
from time import sleep
from typing import List, Iterable, Dict, Generator


class ScrapperBase:
    def __init__(self,
                 href: str,
                 sleep: int = 1,
                 page_sleep: int = 1):
        self.start_href: str = href
        self.regular_sleep: int = sleep
        self.pagination_sleep: int = page_sleep
        self.search_locations: List = []
        self.driver = None

        self.errors = []

        # logger init
        self.logger_base: logging.Logger = logging.getLogger('app.ScrapperBase')
        self.logger_base.info('ScrapperBase init ok')

    def prepare(self):
        """Init search e,g, driver, options, etc."""
        pass

    def starting_point(self):
        """Go to search starting point e.g. main page."""
        pass

    def prepare_for_search(self):
        """Configure search options."""
        pass

    def next_search(self, location):
        """Go to a search page, reset initial params if any."""
        pass

    def get_main_container(self) -> Iterable:
        """Get the container  with search results."""
        pass

    def get_elements(self, container: Iterable) -> Generator[Dict, None, None]:
        """Generator that yields particular search result from container."""
        pass

    def store_element(self, element: Dict):
        """Store particular search element."""
        pass

    def next_page(self) -> bool:
        """Go to the next search result page."""
        pass

    def finish_location(self, location) -> bool:
        """Finish search for location."""
        pass

    def update_locations(self, update):
        """To be implemented in a child."""
        pass

    def finalise_search(self):
        """Final cleanup after all the searches are done."""
        pass

    def scrap(self):
        """Main scrapper orchestrator."""
        if len(self.search_locations) > 0:
            self.logger_base.info('Loop over search locations')
        else:
            return

        self.prepare()
        self.starting_point()
        self.prepare_for_search()

        for location in self.search_locations:
            self.next_search(location)
            self.logger_base.debug('Loop over search results')

            while True:
                container = self.get_main_container()
                for element in self.get_elements(container):
                    self.store_element(element)

                self.logger_base.debug('Go to next page')
                if not self.next_page():
                    break

            self.logger_base.debug(f'Finishing location {location}')
            if not self.finish_location(location):
                self.errors.append(f'Failed to finalize location {location}')

        self.finalise_search()
