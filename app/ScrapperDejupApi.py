import logging
from datetime import datetime
from json import loads, dumps
from os import getenv as ge
from time import sleep
from typing import Iterable, Dict

import requests
from dotenv import load_dotenv

from app.ScrapperDejup import ScrapperDejup

load_dotenv()


def get_scrapper(update: bool = False):
    """Build an API scrapper."""
    load_dotenv()
    scrap_url = ge('DEJUP_SCRAP_URL')
    reg_sleep = ge('REGULAR_SLEEP')
    page_sleep = ge('PAGINATION_SLEEP')
    driver_path = ge('DRIVER_PATH')

    scrapper = ApiScrapper(scrap_url, int(reg_sleep),
                           int(page_sleep), driver_path, update)

    return scrapper


class ApiScrapper(ScrapperDejup):
    def __init__(self, scrap_url, reg_sleep, page_sleep, driver_path, update):
        super().__init__(scrap_url, reg_sleep, page_sleep, driver_path, update)

        self.wait_request_sleep = 5

        self.logger = logging.getLogger('app.ApiScrapper')

        self.payload = {}
        self.last_page = False

        if not self.storage.initialise():
            self.logger.error('Failed to init API client.')
            raise RuntimeError('Failed to initialize API client')

        self.logger.info('ApiScrapper init ok')

    def next_search(self, location):
        self.payload = {}
        self.last_page = False

        del self.driver.requests
        super().next_search(location)
        sleep(self.wait_request_sleep)

        self.logger.info('Looking for the proper request')
        found_request = False
        for request in self.driver.requests:
            if request.url == f'{self.start_href}/recherche':
                context = loads(request.params['context'])
                self.payload = context['poi']
                self.logger.info('Request found')
                found_request = True
                break

        if not found_request:
            self.logger.exception(f'Request not found for location {location}')


    def get_main_container(self):
        url = f'{self.start_href}/pois_service.asp'
        args = {'context': dumps({'poi': self.payload})}
        response = requests.get(url, params=args)

        if response.status_code != 200:
            self.logger.warning(f'Failed request with rc {response.status_code} and {response.text}')
            return []

        self.last_page = response.json()['totalCount'] < response.json()['pageNum'] * response.json()['pageSize']

        return response.json()['pois']

    def get_elements(self, container: Iterable):
        for place in container:
            yield place

    def store_element(self, element: Dict):
        self.data.append(self.parse_item(element))

        if len(self.data) >= self.bunch_size:
            self.store_data()

    def next_page(self) -> bool:
        if self.last_page:
            return False

        sleep(self.pagination_sleep)
        self.payload['pageNum'] += 1

        return True

    def store_data(self):
        self.logger.debug('Store elements with the API call')
        if self.update:
            if not self.storage.update_element_request(self.data):
                self.errors.append(f'Failed to update elements {self.data}')
        else:
            if not self.storage.store_element_request(self.data):
                self.errors.append(f'Failed to post elements {self.data}')
        self.stored += len(self.data)
        self.data = []

    @staticmethod
    def parse_item(place: Dict):
        return {
            'id': int(place['poi']['id']),
            'link': '',
            'x': int(place['poi']['x']),
            'y': int(place['poi']['y']),
            'name': place['poi']['datas']['label'],
            'address': place['poi']['datas']['address1'],
            'address2': place['poi']['datas']['address2'],
            'zipcode': int(place['poi']['datas']['zip']),
            'city': place['poi']['datas']['city'],
            'accepteCheque': bool(place['poi']['datas']['accepteCheque']),
            'accepteCarte3C': bool(place['poi']['datas']['accepteCarte3C']),
            'accepteCarte4C': bool(place['poi']['datas']['accepteCarte4C']),
            'accepteCarte4C2': bool(place['poi']['datas']['accepteCarte4C2']),
            'accepteCarteHybride': bool(place['poi']['datas']['accepteCarteHybride']),
            'upPlus': bool(place['poi']['datas']['upPlus']),
            'categoryName': place['poi']['datas']['categoryName'],
            'updated_at': datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
        }
