import logging
from datetime import datetime, timedelta
from json import dumps
from typing import Dict, List

import requests


class ApiClient:
    """API client to access DB.
    Receives targets and stores the scrapped data.
    """

    def __init__(self, url: str, source: str, user: str, pasw: str):
        self.url = url
        self.source = source

        self.user = user
        self.pasw = pasw

        self.version = '/v1'
        self.location_endpoint = '/locations'

        self.token = ''
        self.token_exp = 0

        self.logger = logging.getLogger('app.ApiClient')

    def initialise(self) -> bool:
        """Request token based on client creds."""
        return self.get_token() != ''

    def get_header(self) -> Dict[str, str]:
        return {'Content-Type': 'application/json',
                'Authorization': f'Bearer {self.get_token()}'}

    def get_token(self) -> str:
        if self.token != '' and self.token_exp > (datetime.now() + timedelta(seconds=10)).timestamp():
            return self.token
        body = {
            'scope': 'read:dejup write:dejup read:dejup_locations write:dejup_locations read:edenred \
            write:edenred read:edenred_locations write:edenred_locations',
            'username': self.user,
            'password': self.pasw
        }
        resp = requests.post(f'{self.url}{self.version}/auth/token', data=body)
        if resp.status_code != 200:
            return ''

        self.token = resp.json().get('access_token')
        self.token_exp = resp.json().get('exp')
        return self.token

    def store_element_request(self, data: List[Dict]) -> bool:
        """Store elements in th DB with PUT."""
        response = requests.post(
            f'{self.url}{self.version}/places{self.source}/batch', headers=self.get_header(), data=dumps(data))
        if response.status_code != 201:
            self.logger.exception(f'Place is not stored, code {response.status_code} body {response.text}')
            return False
        return True

    def update_element_request(self, data: List[Dict]) -> bool:
        """Update elements in th DB with PUT."""
        response = requests.put(
            f'{self.url}{self.version}/places{self.source}/batch', headers=self.get_header(), data=dumps(data))
        if response.status_code != 200:
            self.logger.exception(f'Place is not stored, code {response.status_code} body {response.text}')
            return False
        return True

    def finish_location_request(self, loc_id: int) -> bool:
        """Update index record with the lookup date"""
        response = requests.patch(
            f'{self.url}{self.version}{self.location_endpoint}{self.source}/{loc_id}', headers=self.get_header(),
            data=dumps({'updated_at': f'{datetime.now()}'}))
        if response.status_code != 200:
            self.logger.exception(f'location {loc_id} is not updated, code {response.status_code} body {response.text}')
            return False
        return True

    def update_locations_request(self, update) -> Dict:
        """Request locations w/o a lookup search yet."""
        if not update:
            response = requests.get(
                f'{self.url}{self.version}{self.location_endpoint}{self.source}?query=not_updated',
                headers=self.get_header())
        else:
            response = requests.get(
                f'{self.url}{self.version}{self.location_endpoint}{self.source}?query=outdated&outdated={update}',
                headers=self.get_header())
        if response.status_code != 200:
            self.logger.exception(f'Search location update fail: {response.status_code} {response.text}')
            return {}

        return response.json()
