from os import getenv as ge

from dotenv import load_dotenv

from app.ScrapperEdenredApi import get_scrapper
from test.conftest import get_urls

load_dotenv()
URL = ge('URL')


def test_get_scrapper(mock_api):
    get_scrapper()
    assert len(mock_api.request_history) > 0
    assert f'{URL}/v1/auth/token' in get_urls(mock_api)


INDEX_SAMPLE = [{
    "lat": 48.893132,
    "lon": 2.224915,
    "radius": 0.5,
    "index": 1
}
]


def test_index_lookup(mock_api):
    mock_api.get(f'{URL}/v1/locations/edenred?query=not_updated', status_code=200, json=INDEX_SAMPLE)

    scrapper = get_scrapper()
    scrapper.update_locations(0)
    assert len(scrapper.search_locations) == 1


def test_edenred_scrapper(mock_api):
    mock_api.get(f'{URL}/v1/locations/edenred?query=not_updated', status_code=200, json=INDEX_SAMPLE)
    mock_api.post(f'{URL}/v1/places/edenred/batch', status_code=201)
    mock_api.patch(f'{URL}/v1/locations/edenred/1', status_code=200)

    scrapper = get_scrapper()
    scrapper.update_locations(0)
    scrapper.scrap()

    assert f'{URL}/v1/locations/edenred/1' in get_urls(mock_api)
    assert len(scrapper.errors) == 0


def test_wrong_response(mock_api):
    mock_api.get(f'{URL}/v1/locations/edenred?query=not_updated', status_code=200, json=INDEX_SAMPLE)
    mock_api.post(f'{URL}/v1/places/edenred/batch', status_code=201)
    mock_api.patch(f'{URL}/v1/locations/edenred/1', status_code=200)

    mock_api.get(
        'https://www.edenred.fr/api/outlets?result_level=full&page_size=200&supported_products=CTR_4C&page_index=0&location=48.893132%2C2.224915&radius=0.5',
        status_code=400)

    scrapper = get_scrapper()
    scrapper.update_locations(0)
    scrapper.scrap()

    assert f'{URL}/v1/locations/edenred/1' not in get_urls(mock_api)
    assert len(scrapper.errors) == 1


def test_storerequest_error(mock_api):
    mock_api.get(f'{URL}/v1/locations/edenred?query=not_updated', status_code=200, json=INDEX_SAMPLE)
    mock_api.post(f'{URL}/v1/places/edenred/batch', status_code=401)
    mock_api.patch(f'{URL}/v1/locations/edenred/1', status_code=200)

    scrapper = get_scrapper()
    scrapper.update_locations(0)
    scrapper.scrap()

    assert f'{URL}/v1/locations/edenred/1' not in get_urls(mock_api)
    assert len(scrapper.errors) == 1
