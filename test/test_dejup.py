from os import getenv as ge

from dotenv import load_dotenv

from app.ScrapperDejupApi import get_scrapper as get_scrapper_dejup_api
from app.ScrapperDejup import get_scrapper as get_scrapper_dejup
from test.conftest import get_urls

load_dotenv()
URL = ge('URL')


def test_get_scrapper(mock_api):
    get_scrapper_dejup()
    assert len(mock_api.request_history) > 0
    assert f'{URL}/v1/auth/token' in get_urls(mock_api)


def test_index_lookup(mock_api):
    mock_api.get(f'{URL}/v1/locations/dejup?query=not_updated', status_code=200, json=[{'index': 75000}])

    scrapper = get_scrapper_dejup()
    scrapper.update_locations(0)
    assert len(scrapper.search_locations) == 1


def test_dejup_base(mock_api):
    mock_api.get(f'{URL}/v1/locations/dejup?query=not_updated', status_code=200, json=[{'index': 91190}])
    mock_api.post(f'{URL}/v1/places/dejup/batch', status_code=201)
    mock_api.patch(f'{URL}/v1/locations/dejup/91190', status_code=200)

    scrapper = get_scrapper_dejup()
    scrapper.update_locations(0)
    scrapper.scrap()

    assert f'{URL}/v1/locations/dejup/91190' in get_urls(mock_api)
    assert len(scrapper.errors) == 0


def test_dejup_api(mock_api):
    mock_api.get(f'{URL}/v1/locations/dejup?query=not_updated', status_code=200, json=[{'index': 91190}])
    mock_api.post(f'{URL}/v1/places/dejup/batch', status_code=201)
    mock_api.patch(f'{URL}/v1/locations/dejup/91190', status_code=200)

    scrapper = get_scrapper_dejup_api()
    scrapper.update_locations(0)
    scrapper.scrap()

    assert f'{URL}/v1/locations/dejup/91190' in get_urls(mock_api)
    assert len(scrapper.errors) == 0
